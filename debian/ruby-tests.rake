require 'gem2deb/rake/testtask'

Gem2Deb::Rake::TestTask.new do |t|
  t.libs = ['test']
  unless ENV['AUTOPKGTEST_TMP']
    t.test_files = FileList['test/**/*_test.rb'] + FileList['test/**/test_*.rb']
  else
    # empty list
    t.test_files = FileList[]
  end
end
